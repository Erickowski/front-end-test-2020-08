import React, { useState, useEffect } from "react";
import Axios from "axios";

import Courses from "./components/Courses";
import Nav from "./components/Nav";
import Pages from "./components/Pages";

import "./assets/css/index.css";

export const App = () => {
  const [url, saveUrl] = useState(
    "https://api-staging.prendea.com/api/courses/"
  );
  const [courses, saveCourses] = useState([]);
  const [nextPage, saveNextPage] = useState("");
  const [previousPage, savePreviousPage] = useState("");

  useEffect(() => {
    const getData = async () => {
      const resultado = await Axios.get(url);
      console.log(resultado.data);
      saveNextPage(resultado.data.next);
      savePreviousPage(resultado.data.previous);
      saveCourses(resultado.data.results);
      const nav = document.querySelector(".nav-title-link");
      nav.scrollIntoView({ behavior: "smooth" });
    };
    getData();
  }, [url]);
  return (
    <>
      <Nav />
      <Courses courses={courses} />
      <Pages
        nextPage={nextPage}
        previousPage={previousPage}
        saveUrl={saveUrl}
      />
    </>
  );
};
