import React from "react";

const Pages = ({ nextPage, previousPage, saveUrl }) => {
    return (
        <div className="pages_container">
            {previousPage && (
                <button type="button" onClick={() => saveUrl(previousPage)}>
                    &lt; Anterior
                </button>
            )}
            {nextPage && (
                <button type="button" onClick={() => saveUrl(nextPage)}>
                    Siguiente &gt;
                </button>
            )}
        </div>
    );
};

export default Pages;
