import React from "react";

const Nav = () => {
    return (
        <h2 className="nav-title">
            <a className="nav-title-link" href="https://staging.prendea.com/">
                Inicio
            </a>{" "}
            &gt; Buscar
        </h2>
    );
};

export default Nav;
