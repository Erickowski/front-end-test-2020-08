import React from "react";

import Course from "./Course";
import Loader from "./Loader";

const Courses = ({ courses }) => {
    return (
        <>
            <h1>Encuentra tu curso ideal</h1>
            {courses.length === 0 ? (
                <Loader />
            ) : (
                <div className="courses_container">
                    {courses.map((course) => (
                        <Course key={course.id} course={course} />
                    ))}
                </div>
            )}
        </>
    );
};

export default Courses;
