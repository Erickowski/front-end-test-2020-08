import React from "react";

const Course = ({ course }) => {
    const courseDate = new Date(course.next_lecture_date);
    let dayCourse;
    switch (courseDate.getDay()) {
        case 1:
            dayCourse = "Lunes";
            break;
        case 2:
            dayCourse = "Martes";
            break;
        case 3:
            dayCourse = "Miercoles";
            break;
        case 4:
            dayCourse = "Jueves";
            break;
        case 5:
            dayCourse = "Viernes";
            break;
        case 6:
            dayCourse = "Sabado";
            break;
        case 7:
            dayCourse = "Domingo";
            break;
        default:
            break;
    }
    return (
        <a
            className="course_container"
            href={`https://staging.prendea.com/course/${course.id}/prendea-test`}
            target="_blank"
        >
            <img className="course_img" src={course.picture} alt="" />
            <div className="course_content">
                <p className="course_datetime">
                    {dayCourse} |{" "}
                    {courseDate.toLocaleTimeString("en-US", {
                        hour: "2-digit",
                        minute: "2-digit",
                    })}
                </p>
                <h6 className="course_title">{course.title}</h6>
                <p className="course_age">
                    {course.min_age} - {course.max_age} Años
                </p>
                <div className="course_mentor">
                    <img
                        className="course_mentor_img"
                        src={course.teacher.public_picture}
                        alt=""
                    />
                    <p className="course_mentor_name">
                        {course.teacher.full_name}
                    </p>
                </div>
            </div>
        </a>
    );
};

export default Course;
